﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;


namespace Input
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void IntegerTextBox_OnTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = (TextBox) sender;
            int i;
            if (!int.TryParse(textBox.Text + e.Text, out i) && (e.Text != "-" || textBox.Text != String.Empty))
                e.Handled = true;
        }
        private void DoubleTextBox_OnTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.UserCustomCulture |
                                            CultureTypes.SpecificCultures);

            IEnumerable<NumberFormatInfo> numberFormatInfos = cultures.Select(x => x.NumberFormat);

            if (numberFormatInfos.Select(x => x.NumberDecimalSeparator).Contains(e.Text) && e.Text != CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator)
            {
                TextCompositionManager.StartComposition(
                     new TextComposition(InputManager.Current, textBox, CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                e.Handled = true;
            }

            double d;
            if (!double.TryParse(textBox.Text + e.Text, out d) && (e.Text != "-" || textBox.Text != String.Empty))
                e.Handled = true;
        }

        private void TextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
        }
    }
}
